package com.loadBalancer.students.repository;

import org.springframework.data.repository.CrudRepository;

import com.loadBalancer.students.model.entity.Student;

public interface StudentsRepository extends CrudRepository<Student, Long> {

}
