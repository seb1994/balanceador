package com.loadBalancer.students.controllers;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;


import com.loadBalancer.students.model.entity.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.loadBalancer.students.services.StudentsSrv;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@RestController
public class StudentsController {

    @Value("${config.balanceador.test}")
    private String balanceadorTest;

    @Autowired
    private StudentsSrv service;

    @GetMapping()
    public ResponseEntity<?> getStudents() throws UnknownHostException {
        Map<String, Object> response = new HashMap<String, Object>();
        response.put("Atendido por", balanceadorTest);
        response.put("Ip Server", this.getIpServer());
        response.put("Students", service.findAll());

        return ResponseEntity.ok(response);
    }

    @PostMapping()
    public ResponseEntity<?> createStudent(@RequestBody Student entity) throws UnknownHostException {
        Map<String, Object> response = new HashMap<String, Object>();
        response.put("Atendido por", balanceadorTest);
        response.put("Ip Server", this.getIpServer());
        response.put("Students", service.save(entity));

        return ResponseEntity.status(HttpStatus.CREATED).body(response);
    }

    public ResponseEntity<?> updateStudent(@RequestBody Student entity, @PathVariable Long id) throws UnknownHostException {
        Optional<Student> opt = this.service.findById(id);

        if (opt.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        Student stDB = opt.get();
        stDB.setName(entity.getName());
        stDB.setLastName(entity.getLastName());

        Map<String, Object> response = new HashMap<String, Object>();
        response.put("Atendido por", balanceadorTest);
        response.put("Ip Server", this.getIpServer());
        response.put("Students", service.save(entity));

        return ResponseEntity.status(HttpStatus.CREATED).body(response);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteStudent(@PathVariable Long id) throws UnknownHostException {
        Optional<Student> opt = service.findById(id);

		if(opt.isEmpty()) {
			return ResponseEntity.notFound().build();
		}

		service.delete(id);

		Map<String, Object> response = new HashMap<String, Object>();
		response.put("Atendido por", balanceadorTest);
		response.put("Ip Server", this.getIpServer());

		return ResponseEntity.status(HttpStatus.GONE).body(response);


    }

    public String getIpServer() throws UnknownHostException {
        InetAddress ip = InetAddress.getLocalHost();
        return ip.getHostAddress();
    }


}
