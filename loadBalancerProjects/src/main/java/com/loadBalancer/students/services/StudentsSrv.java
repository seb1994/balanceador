package com.loadBalancer.students.services;

import java.util.Optional;

import com.loadBalancer.students.model.entity.Student;

public interface StudentsSrv {
	
	public Iterable<Student> findAll();
	
	public Optional<Student> findById(Long id);
	
	public Student save(Student entity);
	
	public void delete(Long id);

}
