package com.loadBalancer.students.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.loadBalancer.students.model.entity.Student;
import com.loadBalancer.students.repository.StudentsRepository;

@Service
public class StudentsSrvImpl implements StudentsSrv {
	
	@Autowired
	StudentsRepository repository;

	@Override
	public Iterable<Student> findAll() {
		return repository.findAll();
	}

	@Override
	public Optional<Student> findById(Long id) {
		return repository.findById(id);
	}

	@Override
	public Student save(Student entity) {
		return repository.save(entity);
	}

	@Override
	public void delete(Long id) {
		repository.deleteById(id);
	}

}
